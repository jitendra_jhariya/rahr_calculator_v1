from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.db import connections
from django.db.utils import OperationalError
from .models import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import CalculationsSerializer, PostCalculationsSerializer
from drf_spectacular.utils import extend_schema, OpenApiParameter, OpenApiExample
import json


class CalculationView(APIView):

    @extend_schema(
        parameters=[
            OpenApiParameter("calc_id")
        ],
        request=CalculationsSerializer,
        responses={200: CalculationsSerializer},
    )
    def get(self,request):
        calculation_id = request.query_params.get('calc_id', None)
        if calculation_id:
            print("myod  ",request.query_params.get('calc_id'))
            calcs = Calculations.objects.filter(id=calculation_id)
        else:
            calcs = Calculations.objects.all()
        serializer = CalculationsSerializer(calcs, many=True)
        print(serializer.data)
        response = JsonResponse(serializer.data, content_type='application/json', safe=False)
        return response
        #return Response(serializer.data)

    @extend_schema(
        request=PostCalculationsSerializer,
        responses={201: CalculationsSerializer},
    )
    def post(self, request):
        pass
    #
    # def calculation_detail(request, id):
    #     db_conn = connections['default']
    #     c = db_conn.cursor()
    #     print(c)
    #     try:
    #         c = db_conn.cursor()
    #         connected = True
    #     except Exception as e:
    #         print(e)
    #         connected = False
    #     print(connected)
    #     return JsonResponse({
    #                     "connection_status": "connection - "+str(connected),
    #                     "data_received": str(id)}
    #                     , safe=False)