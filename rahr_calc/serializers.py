from rest_framework import serializers
from rahr_calc.models import Calculations


class CalculationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calculations
        fields = '__all__'
class PostCalculationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calculations
        fields = ("agents", "sales")