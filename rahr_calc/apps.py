from django.apps import AppConfig


class RahrCalcConfig(AppConfig):
    name = 'rahr_calc'
