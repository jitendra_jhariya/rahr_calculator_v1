# Generated by Django 3.1.1 on 2021-08-06 18:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rahr_calc', '0003_delete_users'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='calculations',
            table='Calculations',
        ),
    ]
