from django.db import models


class Users(models.Model):
    name = models.CharField(max_length=90, null=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        db_table = 'Users'


class Calculations(models.Model):
    sales = models.IntegerField(null=True, blank=True)
    deals = models.IntegerField(null=True, blank=True)
    agents = models.IntegerField(null=True, blank=True)
    total_revenue = models.FloatField(null=True, blank=True)
    deal_processing_staff_costs = models.FloatField(null=True, blank=True)
    accounting_staff_costs = models.FloatField(null=True, blank=True)
    accounting_software_costs = models.FloatField(null=True, blank=True)
    deal_system_costs = models.FloatField(null=True, blank=True)
    server_costs = models.FloatField(null=True, blank=True)
    legal_compliance_costs = models.FloatField(null=True, blank=True)
    training_costs = models.FloatField(null=True, blank=True)
    front_desk_service_costs = models.FloatField(null=True, blank=True)
    sales_deal_average_fee = models.FloatField(null=True, blank=True)
    lease_deal_average_fee = models.FloatField(null=True, blank=True)
    monthly_accounting_fee = models.FloatField(null=True, blank=True)
    lonewolf_fee = models.FloatField(null=True, blank=True)
    hours_per_day = models.IntegerField(null=True, blank=True)
    hours_per_year = models.IntegerField(null=True, blank=True)
    profit = models.IntegerField(null=True, blank=True)
    profit_realservus_solution = models.IntegerField(null=True, blank=True)
    #profit_current_brokerage = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.profit)

    class Meta:
        db_table = 'Calculations'
