from lambda_wsgi import response

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sample.settings")

application = get_wsgi_application()

def lambda_handler(event, context):
    return response(application, event, context)
